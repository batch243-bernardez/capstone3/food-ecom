import {Row, Col, Card, Button, Container} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import {useUserContext} from '../hooks/useUserContext';
import Swal from 'sweetalert2';


export default function Cart(){

//const {_id, userId, productId, productName, quantity, totalAmount, purchasedOn} = prop.prop;

const {user} = useUserContext();
const [orders, setOrders] = useState([]);
const [isUpdated, setIsUpdated] = useState(false);

useEffect(() => {
    const fetchOrders = async () => {
        const response = await fetch(`${process.env.REACT_APP_URI}/orders`, {
            headers: {
                "Content-Type" : "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })

        console.log(response);
        const data = await response.json();

        if(!response.ok){
            console.log(data.error);
        }

        if(response.ok){
            console.log(data);
            setOrders(data);
        }
    }

    fetchOrders()

}, [isUpdated])



const checkOut = (cartItemId) => {

    fetch(`${process.env.REACT_APP_URI}/orders/checkout/${cartItemId}`,{
    method: "POST",
    headers: {
        "Content-Type" : "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
    },
    })
    .then(response => response.json())
        .then(data =>{
            Swal.fire({
                title: "Checkout Successfully!",
                icon: "success",
              })
            setIsUpdated(i => !i)
        })
    }
    console.log(orders)


    return(
        <Container>
            <h3 className="text-center text-light mt-5 fw-bold">ORDER SUMMARY</h3>
        <Row className= 'my-3'>
            {orders.map(order => {
                return <Col xs={12} md={6} className='offset-md-3 offset-0 py-2' key={order._id}>
                <Card className="bg-light p-3 ptext-dark bg-opacity-50">
                    
                    <Card.Body className = "">
                        <Row>
                            <div className="col-6 text-start">
                                <p className = "fw-bold">Product Name:</p>
                                <p className = "fw-bold">Quantity:</p>
                                <p className = "fw-bold">Price:</p>
                                <p className = "fw-bold">Total Amount:</p>
                            </div>

                            <div className="col-6 text-end">
                                <p>{order.productName}</p>
                                <p>{order.quantity}</p>
                                <p>{order.productPrice}</p>
                                <p>₱ {order.subtotal}</p>
                            </div>
                        </Row>
                        <br></br>
                        <Button onClick = {() => checkOut(order._id)} variant="outline-light" className = "prodviewbtn yellow1 fw-bold text-dark shadow-sm">Check Out</Button>
                        
                    </Card.Body>
                </Card>
            </Col>
            })}

            
        </Row>
        </Container>

    )

}