// Banner Component: For Home

import {Container, Row, Col, Carousel} from 'react-bootstrap';
import banner1 from '../images/banner1.jpg';
import banner2 from '../images/banner2.jpg';
import banner3 from '../images/banner3.jpg';
import banner4 from '../images/banner4.jpg';
import banner5 from '../images/banner5.jpg';


export default function Banner(){
	return(
		<Container className = "my-5">
			<Row className = "">
				<Col className="">
					<Carousel fade className="rounded shadow-lg ">
					    <Carousel.Item className="carousel-item">
					        <img
					          className=""
					          src={banner1}
					          alt="First slide"
					        />
						</Carousel.Item>

						<Carousel.Item>
					        <img
					          className=""
					          src={banner2}
					          alt="Second slide"
					        />
						</Carousel.Item>

						<Carousel.Item>
					        <img
					          className=""
					          src={banner3}
					          alt="Third slide"
					        />
						</Carousel.Item>

						<Carousel.Item>
					        <img
					          className=""
					          src={banner4}
					          alt="Fourth slide"
					        />
						</Carousel.Item>

						<Carousel.Item>
					        <img
					          className=""
					          src={banner5}
					          alt="Fifth slide"
					        />
						</Carousel.Item>
					</Carousel>
				</Col>
			</Row>
		</Container>

		
		

		
		)
}