//Admin Dashboard contains all the modification: Update, Archive, Unarchive, 

import {Button, Col, Row, Container, Table} from "react-bootstrap";
import {useEffect, useState, useContext} from "react";
import UserContext from "../UserContext";
import {useNavigate, Navigate, Link, useParams} from "react-router-dom";
import Swal from 'sweetalert2';
import { useUserContext } from '../hooks/useUserContext';

export default function AdminDashboard() {
  
  const {user} = useUserContext();
  const [products, setProducts] = useState([]);
  const navigate = useNavigate();
  const [isUpdated, setIsUpdated] = useState(false); //Modified
 

  useEffect(() => {
    fetch(`${process.env.REACT_APP_URI}/products/allProducts`, {
      headers: {"Content-Type" : "application/json", //Modified
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setProducts(data);
      });
  }, [isUpdated]);

  const Unarchive = (id) => {
    fetch(`${process.env.REACT_APP_URI}/products/${id}/unarchived`, {
      method: "PATCH",
      headers: {Authorization: `Bearer ${localStorage.getItem("token")}`},
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Hi", data);

        if (data) {
          Swal.fire({
            title: "Product Successfully Unarchived",
            icon: "success",
            text: "Product is now active again",
          });

          products.map(product => {
              if(product._id === id){
                
                setIsUpdated(false);
              }
            })
        }

        else {
          Swal.fire({
            title: "Product Unarchived Failed",
            icon: "error",
            text: "Failure to activate the product, please try again.",
          });
        }
      });
  }


  function archive(id){
      console.log(id);
      console.log(products)
      fetch(`${process.env.REACT_APP_URI}/products/${id}/archived`, {
        method: "PATCH",
        headers: {"Content-Type" : "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`},
      })
        .then((response) => response.json())
        .then((data) => {
          console.log("Hello", data);

          if (data) {
            Swal.fire({
              title: "Product Successfully Archived",
              icon: "success",
              text: "Product deactivated",
            });

            products.map(product => {
              if(product._id === id){
                console.log("Friday")
                
                setIsUpdated(true);
              }
            })

          }

          else {
            Swal.fire({
              title: "Product Archived Failed",
              icon: "error",
              text: "Failure to deactivate the product, please try again.",
            });
          }
        });
    }

  return (
    <div className="p-5 ">
      <Container className="bg-light rounded shadow-lg">
        <Row className="justify-content-center text-center mx-3 my-3">
        
        <Table striped bordered hover className="m-4 shadow-sm">
          <thead className="bg-black text-light">
            <tr>
              <th>Image</th>
              <th>Category</th>
              <th>Name</th>
              <th>Description</th>
              <th>Price</th>
              <th>Stocks</th>
              <th>Active</th>
              <th>Modify</th>
            </tr>
          </thead>

          <tbody className="text-left">
            {products.map((product) => {
              return (
                <tr key={product._id}>
                  <td><img className="product-img" src={product.image}/></td>
                  <td>{product.category}</td>
                  <td>{product.name}</td>
                  <td>{product.description}</td>
                  <td>{product.price}</td>
                  <td>{product.stocks}</td>
                  <td>{String(product.isActive)}</td>
                  <td>
                    {product.isActive ? (
                      <div className="mx-3 my-3 shadow-sm">
                        <Button
                          onClick = {() => archive(product._id)}
                          variant="outline-danger">
                          Archive
                        </Button>
                      </div>

                    ) : (
                      <div className="mx-3 my-3 shadow-sm">
                        <Button
                          onClick = {() => Unarchive(product._id)}
                          variant="outline-success">
                          Unarchive
                        </Button>
                      </div>
                      
                    )}
                    <div className="mx-3 my-3 yellow1 rounded shadow-sm">
                      <Button
                        as={Link}
                        to={`/adminUpdate/${product._id}`}
                        variant="">
                        Update
                      </Button>
                    </div>
                    
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </Row>
      </Container>
      
    </div>
  );
}