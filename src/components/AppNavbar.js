import {useContext, Fragment} from 'react';
import {Container, Nav, Navbar, Dropdown} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
import { useUserContext } from '../hooks/useUserContext';
import logo from '../images/logo1.png';
import adminLogo from '../images/logo3.png';
import cartlogo from '../images/cart.png';
import Image from 'react-bootstrap/Image'

export default function AppNavbar(){

	const {user} = useUserContext();
	

	return(
		<div className = "nav-container">
			<Navbar className = "yellow1 text-dark fw-bold shadow-lg" expnad="lg">
        <Container fluid>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
	          <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="navbar navbar-expand-lg d-flex">

		          	{
		          		user && user.isAdmin &&
		          		
		          		<Fragment>
				            	<div>
				            		<Navbar.Brand as = {NavLink} to = "/adminDashboard">
		            					<img src={adminLogo} className="adminLogo branding" fluid="true" alt="Logo" />
		            				</Navbar.Brand >
		            			</div>	
		            			<div className="ms-auto navbar-nav">
		            					<Nav.Link as = {NavLink} to = "/adminDashboard">Dashboard</Nav.Link>
		            					<Nav.Link as = {NavLink} to = "/adminViewAll">Products</Nav.Link>
		            			 		<Nav.Link as = {NavLink} to = "/adminCreate">Create</Nav.Link>
		            					{/*<Nav.Link as = {NavLink} to = "/orders">Orders</Nav.Link>*/}
		            					<Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
				            	</div>
		            	</Fragment>
		          	}

		          	{
		          		user && !user.isAdmin &&
		          		<Fragment>
				            	<div>
				            		<Navbar.Brand as = {NavLink} to = "/" className="">
		            					<img src={logo} className="brandlogo branding" alt="Logo" />
		            			</Navbar.Brand>
				            	</div>
		            			<div className="ms-auto navbar-nav">
		            				 	<Nav.Link as = {NavLink} to = "/" className="px-3 pt-3">Home</Nav.Link>
		            				 	<Nav.Link as = {NavLink} to = "/viewProducts" className="px-3 pt-3">Menu</Nav.Link>
		            				 	<Nav.Link as = {NavLink} to = "/logout" className="px-3 pt-3">Logout</Nav.Link>
		            					<Nav.Link as = {NavLink} to = "/cart" className="ps-3">
		            						<img src={cartlogo} width='35' height='35' className="addToCart" alt="Logo" />
		            					</Nav.Link>
		            			</div>
			           		</Fragment>
		          	}
		          	
		          	{
		          		!user &&
		          		<Fragment>
			          			<div>
			          				<Navbar.Brand as = {NavLink} to = "/" className="navbar-brand me-auto">
			            					<img src={logo} className="brandlogo branding" fluid="true" alt="Logo" />
			            			</Navbar.Brand>
			            		</div>
		            			<div className="ms-auto navbar-nav">
		            				<Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
			           				<Nav.Link as = {NavLink} to = "/viewProducts">Menu</Nav.Link>
			           				<Nav.Link as = {NavLink} to = "/signup">Sign Up</Nav.Link>
				           			<Nav.Link as = {NavLink} to = "/login">Log In</Nav.Link>
		            			</div>
			           	</Fragment>
		          	}
		          </Nav>
		      	</Navbar.Collapse>

        </Container>
      </Navbar>
		</div>
		

		)
}