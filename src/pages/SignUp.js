// Signup Page: Signup Non-User

import {Container, Row, Col, Button, Form, Card} from 'react-bootstrap';
import React, {useState, useEffect, useContext} from 'react';
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';


export default function SignUp(){
	
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [userName, setUserName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	const navigate = useNavigate();

	useEffect(() => {
		firstName !== "" && lastName !== "" && mobileNo !== "" && email !== "" && password !== "" && confirmPassword !== "" && password === confirmPassword
		? setIsActive(true)
		: setIsActive(false)
	}, [firstName, lastName, mobileNo, email, password, confirmPassword]);


	const signUpUser = (event) => {
		event.preventDefault();

		fetch(`${process.env.REACT_APP_URI}/user/register`, {
			method: "POST",
			headers: {"Content-Type" : "application/json"},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				userName: userName,
				mobileNo: mobileNo,
				email: email,
				password: password,
				confirmPassword: confirmPassword
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);

			if(data.emailExists){
				Swal.fire({
					title: "Email already exists",
					icon: "error",
					text: "Please enter different email address"
				})

				setPassword("");
				setConfirmPassword("");
			}

			else if(data.mobileNoLength === false){
				Swal.fire({
					title: "Invalid mobile number",
					icon: "error",
					text: "Mobile number must be at least 11 digits"
				})

				setPassword("");
				setConfirmPassword("");
			}

			else{
				Swal.fire({
					title: "Successfully Registered",
					icon: "success",
					text: "Welcome to The House of Crust!"
				})

				navigate('/login')
			}
		})

	}


	return (

		<Container className = "my-4">
			<Row>
				<Col className = "col-md-4 col-8 offset-md-4 offset-2">
					<Card>
						<Card.Body>
							<Form onSubmit = {signUpUser}>
						      <Form.Label className = "fw-bold pt-2">User Details</Form.Label>
						      <Form.Group controlId = "firstName">
						      	<Row className="mx-auto">
						      		<Form.Control
							      		className = "me-1 col"
							      		type = "text"
							      		placeholder = "First Name"
							      		value = {firstName}
							      		onChange = {event => setFirstName(event.target.value)}
							      		required/>
						      		<Form.Control
							      		className = "ms-1 col"
							      		type = "text"
							      		placeholder = "Last Name"
							      		value = {lastName}
							      		onChange = {event => setLastName(event.target.value)}
							      		required/>
						      	</Row>						      	
						      </Form.Group>

						      <Form.Group controlId = "username">
						      	<Form.Control
						      		className = "my-3"
						      		type = "text"
						      		placeholder = "Username"
						      		value = {userName}
						      		onChange = {event => setUserName(event.target.value)}
						      		required/>
						      </Form.Group>

						      <Form.Group controlId = "email">
						      	<Form.Label className = "fw-bold pt-2">Login & Contact Details</Form.Label>
						      	<Form.Control
						      		className = "mb-3"
						      		type = "email"
						      		placeholder = "Email Address"
						      		value = {email}
						      		onChange = {event => setEmail(event.target.value)}
						      		required/>
						      </Form.Group>
						      	
						      <Form.Group controlId = "password">
						      	<Form.Control
						      		className = "mb-3"
						      		type = "password"
						      		placeholder = "Password"
						      		value = {password}
						      		onChange = {event => setPassword(event.target.value)}
						      		required/>
						      </Form.Group>
						      	
						      <Form.Group controlId = "confirmPassword">
						      	<Form.Control
						      		className = "mb-3"
						      		type = "password"
						      		placeholder = "Confirm Password"
						      		value = {confirmPassword}
						      		onChange = {event => setConfirmPassword(event.target.value)}
						      		required/>
						      </Form.Group>
						      	
						      <Form.Group controlId = "mobileNo">
						      	<Form.Control
						      		className = "mb-3"
						      		type = "number"
						      		placeholder = "+63"
						      		value = {mobileNo}
						      		onChange = {event => setMobileNo(event.target.value)}
						      		required/>
						      </Form.Group>

						      <Form.Group className = "mb-3 fw-light" controlId = "formBasicCheckbox">
						        <Form.Check type="checkbox" label="The House of Crust values the Republic Act No. 10173, also known as the Data Privacy Act of 2012. Therefore, by registering, you agree to entrust your information with us." className="opacity-75"/>
						      </Form.Group>
						      
						      <Button
								className = "shadow-sm fw-bold my-2 col-6 "
								variant="warning"
								type="submit"
								disabled = {!isActive}>
							  Sign Up
							</Button>
						    </Form>
						</Card.Body>
					</Card>


					
				</Col>
			</Row>
		</Container>
		)
}