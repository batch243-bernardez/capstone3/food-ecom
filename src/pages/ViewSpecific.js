// VIEW SPECIFIC PRODUCT LEADING TO ADD TO CART

import {Button, Card, Container, Row, Col} from "react-bootstrap";
import {useEffect, useState} from "react";
import {useParams, useNavigate, Link} from "react-router-dom";
import {useUserContext} from '../hooks/useUserContext';
import Swal from 'sweetalert2';



export default function SpecificProduct() {
  const [category, setCategory] = useState("");
  const [image, setImage] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState(0);
  const [price, setPrice] = useState("");
  const [stocks, setStocks] = useState(0);
  
  const {user} = useUserContext();
  const {productId} = useParams();
  const [quantity, setQuantity] = useState(1);
  const navigate = useNavigate();


  useEffect(() => {
    fetch(`${process.env.REACT_APP_URI}/products/${productId}`)
      .then((response) => response.json())
      .then(
        (data) => {
          console.log("ZUITT", data)
          setCategory(data.category);
          setImage(data.image);
          setName(data.name);
          setDescription(data.description);
          setPrice(data.price);
          setStocks(data.stocks);
        },
      );
  }, [productId]);

  async function addToCart(){
    console.log(typeof quantity)

    const response = await fetch(`${process.env.REACT_APP_URI}/orders/addToCart/${productId}`, {
      method: "POST",
      headers: {
        "Content-Type" : "application/json",
        Authorization:  `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        quantity: Number(quantity),
      })
    })

    const data = await response.json();

    if(!response.ok){
      console.log(data.error);
    }

    if(response.ok){
      console.log(data)
      
      if(data.productAddedToCart){
        Swal.fire({
          title: "Successful",
          icon: "success",
          text: "Product added to cart successfully"
        })
      }

      else if(data.isUserAdmin === false){
        Swal.fire({
          title: "Unsuccessful",
          icon: "error",
          text: "Failed to add an item to cart"
        })
      }

      else{
        Swal.fire({
          title: "Failed to add item to cart",
          icon: "error",
          text: "Please login first"
        })

        navigate('/login')
      }
    }
  }

  return (
    <Container>
      <Row className= 'my-5'>
        <Col xs={12} md={4} className='offset-md-4 offset-0'>
          <Card className="bg-light p-2 text-dark bg-opacity-50">
              <Card.Img className="active-product-img" src={image}/>
                <Card.Body>
                  <h5 className="fw-bold">{category}</h5>
                  <p>{description}</p>
                  <h5 className="fw-bold">PHP {price}</h5><br/>
                  <h6 className="">Quantity:</h6>
                  <input type="number" onChange={(e) => setQuantity(e.target.value)} placeholder="quantity" value={quantity} className="col-3"/><br/>
                  <Row>
                    <Button variant="outline-light" className="yellow1 fw-bold text-dark shadow-sm mt-5 col mx-3" onClick = {addToCart}>Add to Cart</Button>
                    <Button variant="outline-light" className="yellow1 fw-bold text-dark shadow-sm mt-5 col mx-3" as = {Link} to = "/viewProducts">Menu</Button>
                  </Row>
                  
              </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
    
  );
}
