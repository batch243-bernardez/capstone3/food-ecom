// Home Page: For Home => Navbar, Admin: Admin Dashboard, User: Banner, Menu/All Products

import {Container} from 'react-bootstrap';
import React, {useContext} from 'react';
import { useUserContext } from '../hooks/useUserContext';
import AdminViewAll from '../components/AdminViewAll';
import Banner from '../components/Banner';

export default function Home(){

	const {user} = useUserContext();
	console.log(user)

	return(
		<Container>
			{
				user && user.isAdmin ? <AdminViewAll/> : <Banner/>
			}
		</Container>
		)
}