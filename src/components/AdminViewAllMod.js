// Admin View All Model

import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import { useUserContext } from '../hooks/useUserContext';
import {Link} from 'react-router-dom';

export default function AdminViewAllMod({product}) {
	

	const {_id, category, image, name, description, price, stocks} = product;

	const [endUsers, setEndUsers] = useState(0);
	const [stocksAvailable, setStocksAvailable] = useState(0)
	const [isAvailable, setIsAvailable] = useState(true)

	const {user} = useUserContext();


	useEffect(()=>{
		if(stocksAvailable === 0){
		setIsAvailable(false)
		}

	}, [stocksAvailable])


	function purchase(){

		if(stocksAvailable === 1){
			alert("Congratulations!")
		} 
		
		setEndUsers(endUsers + 1)
		setStocksAvailable(stocksAvailable - 1)
	}


	return (
		<Container>
			<Row className= 'my-5'>
				<Col xs={12} md={4} className='offset-md-4 offset-0'>
					<Card className="bg-light py-3 px-3 text-dark bg-opacity-50">
					      <Card.Body>
					      	<img className="all-product-img" src={image}/>
					        <h4>{name}</h4>
					        <hr></hr>
					        <h6>{category}</h6>
					        <p>{description}</p>
					        <h5>PHP {price}</h5>
					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		
		)
}

