//MENU PAGE (REGULAR USER AND NON-USER)

import {Fragment, useEffect, useState} from "react";
import AllProducts from "../components/AllProducts";

export default function ViewActive() {
  const [products, setProducts] = useState([]);

  /*useEffect(() => {
    console.log("STRING")
    fetch(`${process.env.REACT_APP_URI}/products/allActiveProducts`)
      .then(response => {
        console.log("YSA", response)
        response.json()
      })
      .then((data) => {
        console.log("PALATANDAAN", data) //err: data is undefined
        setProducts(
          data.map((product) => {
            return (<AllProducts key={product._id} productsProp={product}/>);
          })
        );
      });
  }, [])*/

  useEffect(() => {
    const fetchProducts = async () => {
      const response = await fetch(
        `${process.env.REACT_APP_URI}/products/allActiveProducts`
      );

      const json = await response.json();

      if (!response.ok) {
        console.log(json);
        
      }

      if (response.ok) {
        console.log(json)
        setProducts(
          json.map((product) => {
            return (<AllProducts key={product._id} productsProp={product}/>);
          })
        )
      }
    };

    fetchProducts();
  }, []);

  return (<Fragment>{products}</Fragment>);
}
